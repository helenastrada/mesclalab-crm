import api from './api'

export default {
    getDadosDashboard(cb) {
        api.get('/api/dashboard').then((response) => {
            setTimeout(() => cb(response.data), 100)
        }, (err) => {
            console.log(err)
        })
    }
}