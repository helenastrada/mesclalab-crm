import Vue from 'vue'
import Vuex from 'vuex'
import clientes from './modules/clientes'
import auths from './modules/auths'
import dashboards from './modules/dashboards'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        clientes,
        auths,
        dashboards
    },
    strict: debug
})