import auth from '../../api/auth'

// initial state
const state = {
    token: localStorage.getItem('token'),
    // expiredDate: localStorage.getItem('expiredDate')
};

// getters
const getters = {
    isAuthenticated: state => !!state.token
};

// actions
const actions = {
    doLogin({commit}, mensagem) {
        auth.postSignin(resToken => {
            commit('authToken', resToken)
        }, mensagem)
    },
    doLogout({commit}) {
        commit('authToken', '');
        localStorage.removeItem('token');
        // localStorage.removeItem('expiredDate')
    }
};

// mutations
const mutations = {
    authToken(state, resToken) {
        state.token = resToken
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};