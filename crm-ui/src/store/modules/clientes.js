import cliente from '../../api/cliente'

// initial state
const state = {
    all: []
    ,mCliente: {}
    ,aCliente: {}
};

// getters
const getters = {
    allClientes: state => state.all
    ,oneCliente: state => state.aCliente
}

// actions
const actions = {
    getAllClientes({commit}) {
        cliente.getData(clientes => {
            commit('setClientes', clientes)
        })
    },
    getOneCliente({commit, dispatch}, mensagem) {
        cliente.getOneData(aCliente => {
            commit('setOneCliente', aCliente)
        }, mensagem)
    },
    postCliente({commit, dispatch}, data) {
        cliente.postData(
            mrCliente => {
                commit('setCliente', mrCliente);
            }, data
        )
    },
    putCliente({commit, dispatch}, data) {
        cliente.putData(
            mrCliente => {
                commit('setCliente', mrCliente);
            }, data
        )
    }
};

// mutations
const mutations = {
    setClientes(state, clientes) {
        state.all = clientes
    },
    setCliente(state, cliente) {
        state.mCliente = cliente
    },
    setOneCliente(state, cliente) {
        state.aCliente = cliente
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}