import dashboard from '../../api/dashboard'

// initial state
const state = {
    all: []
}

// getters
const getters = {
    allData: state => state.all
}

// actions
const actions = {
    getAllData({commit}) {
        dashboard.getDadosDashboard(dashs => {
            commit('setData', dashs)
        })
    }
}

// mutations
const mutations = {
    setData(state, dashs) {
        state.all = dashs
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}