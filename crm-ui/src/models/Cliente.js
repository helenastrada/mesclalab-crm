export class Cliente {
    constructor({razaoSocial = '', nomeFantasia = '', cnpj = '', id = null} = {}) {
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.cnpj = cnpj;
        this.id = id;
    }
}

export function createCliente(data) {
    return Object.freeze(new Cliente(data));
}
