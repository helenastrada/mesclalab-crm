package br.com.mesclalab.crmapi.service.implementation;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.Role;
import br.com.mesclalab.crmapi.repository.RoleRepository;
import br.com.mesclalab.crmapi.service.RoleService;

@Service
@Transactional
public class IRoleService implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Role findByLabel(String label) {
		return roleRepository.findByLabel(label);
	}

	@Override
	public void saveRole(Role role) {
		roleRepository.save(role);
		
	}

}
