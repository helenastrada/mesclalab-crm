package br.com.mesclalab.crmapi.entity;

public enum StatusVenda {

	ORCAMENTO("Or�amento")
	, EMITIDA("Emitida")
	, CANCELADA("Cancelada");

	private String descricao;

	StatusVenda(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}