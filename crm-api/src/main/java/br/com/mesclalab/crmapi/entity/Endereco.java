package br.com.mesclalab.crmapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "enderecos")
// @Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Endereco {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
//	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	@JoinColumn(name = "cliente_id", nullable = false)
//	private Cliente cliente;

	private String logradouro;

	private String numero;

	private String complemento;

	private String bairro;

	@Size(min = 8, max = 8)
	@Column(name = "cep", length = 8)
	private String cep;

	@OneToOne
	@JoinColumn(name = "cidade_id", nullable = false)
	private Cidade cidade;

}
