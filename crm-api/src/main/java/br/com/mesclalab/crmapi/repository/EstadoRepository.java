package br.com.mesclalab.crmapi.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mesclalab.crmapi.entity.Estado;

@Repository
@Transactional
public interface EstadoRepository extends JpaRepository<Estado, Long> {

	@Query("SELECT e FROM Estado e WHERE e.id = :id")
	Estado buscarEstadoPorId(@Param("id") Long id);

	@Query("SELECT e FROM Estado e WHERE e.nomeEstado = :nome")
	Estado buscarEstadoPorNome(@Param("nome") String nome);

}
