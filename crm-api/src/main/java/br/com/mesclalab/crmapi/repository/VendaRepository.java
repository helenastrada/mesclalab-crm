package br.com.mesclalab.crmapi.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mesclalab.crmapi.entity.Venda;

@Repository
@Transactional
public interface VendaRepository extends JpaRepository<Venda, Long>{

	@Query("SELECT v FROM Venda v WHERE v.cliente.id = :id")
	Optional<List<Venda>> buscarVendasDoCliente(@Param("id") Long id);
	
	@Procedure(name = "calcularValorTotalDaVenda")
	Double calcularValorTotalDaVenda(@Param("idVenda") Long id);
	
	@Query("SELECT sum(v.valorTotal) FROM Venda v")
	Double totalVendas();
	
}
