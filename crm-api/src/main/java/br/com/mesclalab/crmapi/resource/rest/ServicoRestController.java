package br.com.mesclalab.crmapi.resource.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.mesclalab.crmapi.entity.Servico;
import br.com.mesclalab.crmapi.service.ServicoService;

@PreAuthorize("hasAuthority('ADMINISTRADOR')")
@SuppressWarnings("rawtypes")
@RestController
public class ServicoRestController {

	private final static String SERVICOS_URL = "/api/servicos";
	private final static String SERVICOS_PORID_URL = "/api/servicos/{id}";

	private ServicoService servicoService;

	@Autowired
	public void setServicoService(ServicoService servicoService) {
		this.servicoService = servicoService;
	}

	// @PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@GetMapping(SERVICOS_URL)
	public ResponseEntity listarServicos() {
		return ResponseEntity.ok(servicoService.buscarTodosServicos());
	}

	// @PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@PostMapping(SERVICOS_URL)
	public ResponseEntity salvarNovoServico(@Valid @RequestBody Servico servico) {
		return ResponseEntity.ok(servicoService.salvarServico(servico));
	}

	// @PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@PutMapping(SERVICOS_PORID_URL)
	public ResponseEntity atualizarServico(@PathVariable Long id, @Valid @RequestBody Servico servico) {
		
		Servico servicoAtualizado = servicoService.atualizarServico(id, servico);
		
		return ResponseEntity.ok(servicoService.atualizarServico(id, servicoAtualizado));
	}

	// @PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@DeleteMapping(SERVICOS_URL)
	public ResponseEntity deletarServico(@PathVariable("id") Long id) {
		if (servicoService.buscarPorId(id) != null) {
			servicoService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// @PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@GetMapping(SERVICOS_PORID_URL)
	public ResponseEntity buscarUmServico(@PathVariable("id") Long id) {
		return ResponseEntity.ok(servicoService.buscarPorId(id));
	}

}
