package br.com.mesclalab.crmapi.security.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Helena Checks JWT validity
 */
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

	private UserDetailsService userDetailsService;
	private JwtUtil jwtTokenUtil;

	@Value("${auth.header}")
	private String tokenHeader;

	/**
	 * Injects UserDetailsService instance
	 * 
	 * @param userDetailsService
	 *            to inject
	 */
	@Autowired
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	/**
	 * Injects JwtUtil instance
	 * 
	 * @param jwtUtil
	 *            to inject
	 */
	@Autowired
	public void setJwtTokenUtil(JwtUtil jwtUtil) {
		this.jwtTokenUtil = jwtUtil;
	}

	/**
	 * Checks if JWT present and valid
	 * 
	 * @param request
	 *            with JWT
	 * @param response
	 * @param chain
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		String tokenReceived = request.getHeader(this.tokenHeader);

		String authToken = null;

		if (tokenReceived != null) {

			if (tokenReceived.matches("(Bearer)\\s((.*)\\.(.*)\\.(.*))")) {
				try {
					authToken = tokenReceived.split(" ")[1];
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

			}
		}

		String username = jwtTokenUtil.getUsernameFromToken(authToken);

		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

			if (jwtTokenUtil.validateToken(authToken, userDetails)) {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				logger.info("Usu�rio autenticado: " + username + ", aplicando contexto seguro.");
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}
		
		chain.doFilter(request, response);
	}

}
