package br.com.mesclalab.crmapi.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.repository.ClienteRepository;
import br.com.mesclalab.crmapi.repository.ServicoRepository;
import br.com.mesclalab.crmapi.repository.VendaRepository;
import br.com.mesclalab.crmapi.service.AdminService;

@Service
public class IAdminService implements AdminService {
	
	@Autowired
	private ServicoRepository servicoRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private VendaRepository vendaRepository;

	@Override
	public int totalDeClientes() {
		return clienteRepository.totalDeClientes();
	}

	@Override
	public int totalDeServicos() {
		return servicoRepository.totalDeServicos();
	}

	@Override
	public Double totalVendas() {
		return vendaRepository.totalVendas();
	}

}
