package br.com.mesclalab.crmapi.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.Estado;

@Service
public interface EstadoService {

	void salvarEstado(Estado estado);

	Estado buscarPorId(Long id);

	Estado buscarPorNome(String nome);

	List<Estado> listarTodos();
	
	void atualizarEstado(Long id, Estado estado);

}
