package br.com.mesclalab.crmapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.User;

@Service
public interface UserService {
	User save(User user);

	void delete(Long id);

	List<User> findAll();

	Optional<User> findById(Long id);

	User findByEmail(String email);

//	User findByName(String name);
	
	User findByUsername(String username);
}
