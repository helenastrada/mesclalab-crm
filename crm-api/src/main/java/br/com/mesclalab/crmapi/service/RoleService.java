package br.com.mesclalab.crmapi.service;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.Role;

@Service
public interface RoleService {

	Role findByLabel(String label);
	
	void saveRole(Role role);
	
}
