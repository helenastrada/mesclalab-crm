package br.com.mesclalab.crmapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "A senha est� incorreta.")
public class InvalidPasswordException extends RuntimeException {
}
