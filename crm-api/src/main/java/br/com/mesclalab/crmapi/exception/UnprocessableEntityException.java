package br.com.mesclalab.crmapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Entitdade mal formatada.")
public class UnprocessableEntityException extends RuntimeException {

}
