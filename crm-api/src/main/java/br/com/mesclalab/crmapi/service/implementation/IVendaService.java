package br.com.mesclalab.crmapi.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.Venda;
import br.com.mesclalab.crmapi.repository.VendaRepository;
import br.com.mesclalab.crmapi.service.VendaService;

@Service
public class IVendaService implements VendaService {

	private VendaRepository vendaRepository;
	
	@Autowired
	public void setVendaRepository(VendaRepository vendaRepository) {
		this.vendaRepository = vendaRepository;
	}
	
	@Override
	public Venda salvar(Venda venda) {
		return vendaRepository.save(venda);
	}

	@Override
	public void deletarVenda(Long id) {
		vendaRepository.deleteById(id);
	}

	@Override
	public List<Venda> buscarTodasAsVendas() {
		return vendaRepository.findAll();
	}

	@Override
	public Optional<Venda> findById(Long id) {
		return vendaRepository.findById(id);
	}

	@Override
	public Optional<List<Venda>> buscarPorIdDoCliente(Long idCliente) {
		return vendaRepository.buscarVendasDoCliente(idCliente);
	}

	@Override
	public Double calcularValorTotalDaVenda(Long id) {
		return vendaRepository.calcularValorTotalDaVenda(id);
	}

}
