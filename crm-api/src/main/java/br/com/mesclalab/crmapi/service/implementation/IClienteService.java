package br.com.mesclalab.crmapi.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.Cliente;
import br.com.mesclalab.crmapi.exception.EntityAlreadyExistsException;
import br.com.mesclalab.crmapi.exception.EntityNotFoundException;
import br.com.mesclalab.crmapi.repository.ClienteRepository;
import br.com.mesclalab.crmapi.repository.EnderecoRepository;
import br.com.mesclalab.crmapi.service.ClienteService;

@Service
public class IClienteService implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;

	@Override
	public Cliente salvar(Cliente cliente) throws EntityAlreadyExistsException {
		
		if (buscarPorCnpj(cliente.getCnpj()) != null) {
			throw new EntityAlreadyExistsException();
		}
		
		if (cliente.getEndereco() != null) {
			enderecoRepository.save(cliente.getEndereco());
		}
		
		return clienteRepository.save(cliente);
	}

	@Override
	public void delete(Long id) {
		clienteRepository.deleteById(id);
	}

	@Override
	public List<Cliente> buscarTodosClientes() {
		return clienteRepository.findAll();
	}

	@Override
	public Optional<Cliente> findById(Long id) {
		return clienteRepository.findById(id);
	}

	@Override
	public List<Cliente> findByUser(Long id) {
		return clienteRepository.findByUser(id);
	}

	@Override
	public Cliente buscarPorCnpj(String cnpj) {
		return clienteRepository.buscarPorCnpj(cnpj);
	}

	@Override
	public Cliente atualizarCliente(Cliente cliente) {
		Cliente clienteSalvo = buscarPorId(cliente.getId());
		
		if (clienteSalvo == null) {
			throw new EntityNotFoundException();
		}
				
		BeanUtils.copyProperties(cliente, clienteSalvo, "id");
		
		return clienteRepository.save(clienteSalvo);
	}

	@Override
	public Cliente buscarPorId(Long id) {
		return clienteRepository.buscarPorId(id);
	}

}
