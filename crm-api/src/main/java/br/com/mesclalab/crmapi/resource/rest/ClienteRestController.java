package br.com.mesclalab.crmapi.resource.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.mesclalab.crmapi.entity.Cliente;
import br.com.mesclalab.crmapi.exception.EntityAlreadyExistsException;
import br.com.mesclalab.crmapi.security.auth.JwtUser;
import br.com.mesclalab.crmapi.service.ClienteService;

@SuppressWarnings("rawtypes")
@RestController
public class ClienteRestController {

	public final static String CLIENTES_URL = "/api/clientes";
	public final static String CLIENTES_ID_URL = "/api/clientes/{id}";
	public final static String CLIENTES_SEC_URL = "/api/clientes/logged";

	private ClienteService clienteService;

	@Autowired
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@GetMapping(CLIENTES_URL)
	public ResponseEntity listClients() {
		return ResponseEntity.ok(clienteService.buscarTodosClientes());
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@GetMapping(CLIENTES_ID_URL)
	public ResponseEntity listOneClient(@PathVariable Long id) {
		return ResponseEntity.ok(clienteService.findById(id));
	}
	
	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@PostMapping(CLIENTES_URL)
	public ResponseEntity salvarNovoCliente(@RequestBody Cliente cliente) {
		
		try {
			return ResponseEntity.ok(clienteService.salvar(cliente));
		} catch (EntityAlreadyExistsException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@PutMapping(CLIENTES_URL)
	public ResponseEntity atualizarCliente(@RequestBody Cliente cliente) {
		
		try {
			return ResponseEntity.ok(clienteService.atualizarCliente(cliente));
		} catch (EntityAlreadyExistsException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	@PreAuthorize("hasAuthority('COMUM')")
	@GetMapping(CLIENTES_SEC_URL)
	public ResponseEntity listarRegistroDeUmUsuario() {
		JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return ResponseEntity.ok(clienteService.findByUser(user.getId()));
	}

}
