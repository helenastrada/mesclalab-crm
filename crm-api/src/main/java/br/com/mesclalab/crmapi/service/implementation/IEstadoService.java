package br.com.mesclalab.crmapi.service.implementation;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.Estado;
import br.com.mesclalab.crmapi.exception.EntityNotFoundException;
import br.com.mesclalab.crmapi.repository.EstadoRepository;
import br.com.mesclalab.crmapi.service.EstadoService;

@Service
public class IEstadoService implements EstadoService {

	private EstadoRepository estadoRepository;

	@Autowired
	public void setEstadoRepository(EstadoRepository estadoRepository) {
		this.estadoRepository = estadoRepository;
	}

	@Override
	public void salvarEstado(Estado estado) {
		estadoRepository.save(estado);

	}

	@Override
	public Estado buscarPorId(Long id) {
		return estadoRepository.buscarEstadoPorId(id);
	}

	@Override
	public Estado buscarPorNome(String nome) {
		return estadoRepository.buscarEstadoPorNome(nome);
	}

	@Override
	public List<Estado> listarTodos() {
		return estadoRepository.findAll();
	}

	@Override
	public void atualizarEstado(Long id, Estado estado) {
		Estado estadoSalvo = buscarPorId(id);

		if (estadoSalvo == null) {
			throw new EntityNotFoundException();
		}

		BeanUtils.copyProperties(estado, estadoSalvo, "id");

		estadoRepository.save(estadoSalvo);
	}

}
