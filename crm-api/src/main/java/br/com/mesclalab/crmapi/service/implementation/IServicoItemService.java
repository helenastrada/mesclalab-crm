package br.com.mesclalab.crmapi.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.ServicoItem;
import br.com.mesclalab.crmapi.repository.ServicoItemRepository;
import br.com.mesclalab.crmapi.service.ServicoItemService;

@Service
public class IServicoItemService implements ServicoItemService {

	private ServicoItemRepository servicoItemRepository;

	@Autowired
	public void setServicoItemRepository(ServicoItemRepository servicoItemRepository) {
		this.servicoItemRepository = servicoItemRepository;
	}

	@Override
	public ServicoItem save(ServicoItem servicoItem) {
		return servicoItemRepository.save(servicoItem);
	}

	@Override
	public void delete(Long id) {
		servicoItemRepository.deleteById(id);
	}

	@Override
	public List<ServicoItem> buscarTodosOsServicosItens() {
		return servicoItemRepository.findAll();
	}

	@Override
	public Optional<ServicoItem> findById(Long id) {
		return servicoItemRepository.findById(id);
	}

}
