package br.com.mesclalab.crmapi.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mesclalab.crmapi.entity.Cidade;

@Repository
@Transactional
public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	@Query("SELECT c FROM Cidade c WHERE c.id = :id")
	Cidade buscarCidadePorId(@Param("id") Long id);
	
	@Query("SELECT c FROM Cidade c WHERE c.nomeCidade = :nome")
	Cidade buscarCidadePorNome(@Param("nome") String nome);
	
}
