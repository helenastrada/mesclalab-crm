package br.com.mesclalab.crmapi.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mesclalab.crmapi.entity.ServicoItem;

@Repository
@Transactional
public interface ServicoItemRepository extends JpaRepository<ServicoItem, Long>{

}
