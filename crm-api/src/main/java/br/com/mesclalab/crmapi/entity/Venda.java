package br.com.mesclalab.crmapi.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "vendas")
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedStoredProcedureQuery(name = "calcularValorTotalDaVenda", procedureName = "sp_calcularValorTotalDaVenda", parameters = {
		@StoredProcedureParameter(name = "idVenda", mode = ParameterMode.IN, type = Long.class),
		@StoredProcedureParameter(name = "valorTotal", mode = ParameterMode.OUT, type = Float.class), })
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Venda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "valor_total")
	private Float valorTotal;

	@Column(name = "valor_desconto")
	private BigDecimal valorDesconto;

	@Column(name = "observacao_venda")
	private String observacaoVenda;

	// @JsonIgnore
	@OneToMany(mappedBy = "venda", fetch = FetchType.EAGER)
	// @JsonManagedReference
	private List<ServicoItem> servicosItens = new ArrayList<>();

	@OneToOne
	@JsonIgnore
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "usuario_id")
	private User usuario;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "data_venda")
	private Date dataVenda;

	@Enumerated(EnumType.STRING)
	private StatusVenda status = StatusVenda.ORCAMENTO;

}
