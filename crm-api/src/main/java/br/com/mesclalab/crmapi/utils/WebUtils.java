package br.com.mesclalab.crmapi.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class WebUtils {

	public Map<String, String> mapaDe(String chave, String valor) {
		Map<String, String> mapa = new HashMap<>();
		mapa.put(chave, valor);

		return mapa;
	}

}
