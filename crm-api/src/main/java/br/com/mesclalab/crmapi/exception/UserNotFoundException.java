package br.com.mesclalab.crmapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Usu�rio n�o encontrado.")
public class UserNotFoundException extends RuntimeException {
}
