package br.com.mesclalab.crmapi.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crmapi.entity.Cidade;

@Service
public interface CidadeService {

	void salvarCidade(Cidade cidade);
	
	Cidade buscarPorNome(String nomeCidade);
	
	void alterarCidade(Long id, Cidade cidade);
	
	List<Cidade> buscarCidades();
	
	Cidade buscarPorId(Long id);
	
}
