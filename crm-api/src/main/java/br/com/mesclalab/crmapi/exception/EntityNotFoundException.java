package br.com.mesclalab.crmapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Entidade n�o encontrada.")
public class EntityNotFoundException extends RuntimeException {

}
