package br.com.mesclalab.crm.resource.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.mesclalab.crm.entity.Cliente;
import br.com.mesclalab.crm.exception.EntityAlreadyExistsException;
import br.com.mesclalab.crm.security.auth.JwtUser;
import br.com.mesclalab.crm.service.ClienteService;

@SuppressWarnings("rawtypes")
@RestController
public class ClienteRestController {

	public final static String CLIENTES_URL = "/api/clientes";
	public final static String CLIENTES_SEC_URL = "/api/clientes/logged";

	private ClienteService clienteService;

	@Autowired
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@GetMapping(CLIENTES_URL)
	public ResponseEntity listClients() {
		return ResponseEntity.ok(clienteService.buscarTodosClientes());
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@PostMapping(CLIENTES_URL)
	public ResponseEntity salvarNovoCliente(@RequestBody Cliente cliente) {
		
		try {
			return ResponseEntity.ok(clienteService.salvar(cliente));
		} catch (EntityAlreadyExistsException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@PreAuthorize("hasAuthority('COMUM')")
	@GetMapping(CLIENTES_SEC_URL)
	public ResponseEntity listarRegistroDeUmUsuario() {
		JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return ResponseEntity.ok(clienteService.findByUser(user.getId()));
	}

}
