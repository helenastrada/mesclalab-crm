package br.com.mesclalab.crm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "A entidade j� existe.")
public class EntityAlreadyExistsException extends RuntimeException {
}
