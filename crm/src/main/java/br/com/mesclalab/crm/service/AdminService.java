package br.com.mesclalab.crm.service;

import org.springframework.stereotype.Service;

@Service
public interface AdminService {
	
	int totalDeClientes();
	
	int totalDeServicos();
	
	Double totalVendas();

}
