package br.com.mesclalab.crm.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mesclalab.crm.entity.Cliente;

@Repository
@Transactional
public interface ClienteRepository extends JpaRepository <Cliente, Long> {

	@Query("SELECT c FROM Cliente c WHERE c.user.id = :id")
	List<Cliente> findByUser(@Param("id") Long id);
	
	@Query("SELECT c FROM Cliente c WHERE c.cnpj = :cnpj")
	Cliente buscarPorCnpj(@Param("cnpj") String cnpj);
	
	@Query("SELECT COUNT(c) FROM Cliente c")
	int totalDeClientes();
}
