package br.com.mesclalab.crm.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mesclalab.crm.entity.Servico;

@Repository
@Transactional
public interface ServicoRepository extends JpaRepository<Servico, Long> {

	Servico findByNomeServico(String nomeServico);
	
	@Query("SELECT COUNT(s) FROM Servico s")
	int totalDeServicos();
	
	@Query("FROM Servico s WHERE s.id = :id")
	Servico buscarPorId(@Param(value = "id") Long id);
	
}
