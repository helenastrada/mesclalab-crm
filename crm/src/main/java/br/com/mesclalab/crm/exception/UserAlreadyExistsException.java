package br.com.mesclalab.crm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Usu�rio com este nome ou e-mail j� existe.")
public class UserAlreadyExistsException extends RuntimeException {
}
