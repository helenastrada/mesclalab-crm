package br.com.mesclalab.crm.service;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crm.entity.Role;

@Service
public interface RoleService {

	Role findByLabel(String label);
	
	void saveRole(Role role);
	
}
