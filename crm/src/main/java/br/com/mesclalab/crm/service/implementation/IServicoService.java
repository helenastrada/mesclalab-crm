package br.com.mesclalab.crm.service.implementation;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crm.entity.Servico;
import br.com.mesclalab.crm.exception.EntityNotFoundException;
import br.com.mesclalab.crm.repository.ServicoRepository;
import br.com.mesclalab.crm.service.ServicoService;

/**
 * @author Administrador
 *
 */
@Service
public class IServicoService implements ServicoService {

	private ServicoRepository servicoRepository;

	@Autowired
	public void setServicoRepository(ServicoRepository servicoRepository) {
		this.servicoRepository = servicoRepository;
	}

	@Override
	public Servico salvarServico(Servico servico) {
		return servicoRepository.save(servico);
	}

	@Override
	public void delete(Long id) {
		servicoRepository.deleteById(id);
	}

	@Override
	public List<Servico> buscarTodosServicos() {
		return servicoRepository.findAll();
	}

	@Override
	public Servico buscarPorId(Long id) {
		return servicoRepository.buscarPorId(id);
	}

	@Override
	public Servico findByNomeServico(String nomeServico) {
		return servicoRepository.findByNomeServico(nomeServico);
	}

	@Override
	public Servico atualizarServico(Long id, Servico servico) {
		Servico servicoSalvo = buscarPorId(id);
		
		if (servicoSalvo == null) {
			throw new EntityNotFoundException();
		}
				
		BeanUtils.copyProperties(servico, servicoSalvo, "id");
				
		return servicoRepository.save(servicoSalvo);
	}

}
