package br.com.mesclalab.crm.entity;

public enum TipoTelefone {
	
	CELULAR("Celular")
	, RESIDENCIAL("Residencial")
	, COMERCIAL("comercial");

	private String descricao;

	TipoTelefone(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
