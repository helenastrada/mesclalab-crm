package br.com.mesclalab.crm.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crm.entity.Venda;

@Service
public interface VendaService {
	
	Venda salvar(Venda venda);

	void deletarVenda(Long id);

	List<Venda> buscarTodasAsVendas();

	Optional<Venda> findById(Long id);
	
	Optional<List<Venda>> buscarPorIdDoCliente(Long idCliente);
	
	Double calcularValorTotalDaVenda(Long id);

}
