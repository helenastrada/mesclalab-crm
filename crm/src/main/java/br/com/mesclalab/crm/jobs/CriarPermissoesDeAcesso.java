package br.com.mesclalab.crm.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import br.com.mesclalab.crm.entity.Cidade;
import br.com.mesclalab.crm.entity.Estado;
import br.com.mesclalab.crm.entity.Role;
import br.com.mesclalab.crm.entity.User;
import br.com.mesclalab.crm.service.CidadeService;
import br.com.mesclalab.crm.service.EstadoService;
import br.com.mesclalab.crm.service.RoleService;
import br.com.mesclalab.crm.service.UserService;

@Component
public class CriarPermissoesDeAcesso implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private EstadoService estadoService;
	
	@Autowired
	private CidadeService cidadeService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {

		// CRIAR NOVA PERMISS�O DE ACESSO
		Role role = new Role();
		role.setId(1L);
		role.setLabel("ADMINISTRADOR");

		if (roleService.findByLabel(role.getLabel()) != null) {
			// n�o faz nada
		} else {
			// salva uma nova regra
			roleService.saveRole(role);
		}
		
		// CRIAR NOVO USU�RIO
		User user = new User();
		user.setId(1L);
		user.setEmail("admin@admin.com");
		user.setPassword("administrador");
		user.setRole(roleService.findByLabel("ADMINISTRADOR"));
		user.setUsername("administrador");
		
		if (userService.findByEmail(user.getEmail()) != null) {
			// n�o faz nada
		} else {
			// salva o novo usu�rio padr�o
			userService.save(user);
		}
		
		// CRIAR UM ESTADO PARA TESTE
		Estado estado = new Estado();
		estado.setId(1L);
		estado.setNomeEstado("SP");
		
		if (estadoService.buscarPorNome(estado.getNomeEstado()) != null) {
			
		} else {
			estadoService.salvarEstado(estado);
		}
		
		// CRIAR UMA CIDADE PARA TESTE
		Cidade cidade = new Cidade();
		cidade.setId(1L);
		cidade.setNomeCidade("S�o Paulo");
		cidade.setEstado(estado);
		
		if (cidadeService.buscarPorNome(cidade.getNomeCidade()) != null) {
			
		} else {
			cidadeService.salvarCidade(cidade);
		}

	}

}
