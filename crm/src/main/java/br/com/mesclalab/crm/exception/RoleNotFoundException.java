package br.com.mesclalab.crm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Permiss�o n�o encontrada.")
public class RoleNotFoundException extends RuntimeException {

}
