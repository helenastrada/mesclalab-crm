package br.com.mesclalab.crm.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crm.entity.ServicoItem;

@Service
public interface ServicoItemService {

	ServicoItem save(ServicoItem servicoItem);

	void delete(Long id);

	List<ServicoItem> buscarTodosOsServicosItens();

	Optional<ServicoItem> findById(Long id);
	
}
