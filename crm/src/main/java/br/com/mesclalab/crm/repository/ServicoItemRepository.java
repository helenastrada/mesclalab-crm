package br.com.mesclalab.crm.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mesclalab.crm.entity.ServicoItem;

@Repository
@Transactional
public interface ServicoItemRepository extends JpaRepository<ServicoItem, Long>{

}
