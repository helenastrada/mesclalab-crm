package br.com.mesclalab.crm.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crm.entity.Servico;

@Service
public interface ServicoService {

	Servico salvarServico(Servico servico);

	void delete(Long id);

	List<Servico> buscarTodosServicos();

	Servico buscarPorId(Long id);

	Servico findByNomeServico(String nomeServico);
	
	Servico atualizarServico(Long id, Servico servico);

}
