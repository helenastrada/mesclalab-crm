package br.com.mesclalab.crm.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "clientes")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nome_fantasia", length = 150)
	private String nomeFantasia;

	@Column(nullable = false, name = "razao_social", length = 150)
	private String razaoSocial;

	@Column(unique = true, nullable = false, length = 14)
	private String cnpj;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
	@CreationTimestamp
	@Column(name = "criado_em")
	private LocalDateTime criadoEm;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
	@UpdateTimestamp
	@Column(name = "atualizado_em")
	private Date atualizadoEm;

	@OneToOne
	@JoinColumn(name = "user_id", nullable = true, unique = true)
	private User user;
	
	@OneToOne
	@JoinColumn(name = "endereco_id", nullable = true)
	private Endereco endereco;
	
//	@OneToOne
//	@JoinColumn(name = "contato_id", nullable = false)
//	private Contato contato;
	
//	@Embedded
//	private Endereco endereco;

	@OneToMany(mappedBy = "cliente", fetch = FetchType.EAGER)
	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	private List<Contato> contatos = new ArrayList<>();

}
