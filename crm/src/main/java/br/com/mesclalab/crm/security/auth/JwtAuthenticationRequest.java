package br.com.mesclalab.crm.security.auth;

import lombok.Data;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data
public class  JwtAuthenticationRequest implements Serializable {

    private String username;
    private String email;
    private String password;
    private String label;

    public JwtAuthenticationRequest() {
        super();
    }

    public JwtAuthenticationRequest(String username, String email, String password, String label) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.label = label;
    }
    
}
