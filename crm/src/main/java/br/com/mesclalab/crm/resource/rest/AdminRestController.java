package br.com.mesclalab.crm.resource.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mesclalab.crm.service.AdminService;
import net.minidev.json.JSONObject;

@PreAuthorize("hasAuthority('ADMINISTRADOR')")
@SuppressWarnings("rawtypes")
@RestController
public class AdminRestController {
	
	private final static String DASHBOARD_URL = "/api/dashboard";
	
	private AdminService adminService;

	@Autowired
	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}

	@GetMapping(DASHBOARD_URL)
	public ResponseEntity listarDadosDashboard() {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("qtdClientes", adminService.totalDeClientes());
		jsonObject.put("qtdServicos", adminService.totalDeServicos());
		jsonObject.put("totalVendas", String.format("%1$,.2f", adminService.totalVendas()));
		return ResponseEntity.ok(jsonObject);
	}

}
