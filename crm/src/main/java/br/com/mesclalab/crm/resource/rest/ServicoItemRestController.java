package br.com.mesclalab.crm.resource.rest;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.mesclalab.crm.entity.ServicoItem;
import br.com.mesclalab.crm.service.ServicoItemService;

@PreAuthorize("hasAuthority('ADMINISTRADOR')")
@SuppressWarnings("rawtypes")
@RestController
public class ServicoItemRestController {

	private final static String SERVICOS_ITENS_URL = "/api/servicosItens";
	private final static String SERVICOS_ITENS_PORID_URL = "/api/servicosItens/{id}";

	private ServicoItemService servicoItemService;

	@Autowired
	public void setServicoItemService(ServicoItemService servicoItemService) {
		this.servicoItemService = servicoItemService;
	}

	@GetMapping(SERVICOS_ITENS_URL)
	public ResponseEntity listarVendas() {
		return ResponseEntity.ok(servicoItemService.buscarTodosOsServicosItens());
	}

	@PostMapping(SERVICOS_ITENS_URL)
	public ResponseEntity cadastrarNovoItemDeServico(@Valid @RequestBody ServicoItem servicoItem) {
		return ResponseEntity.ok(servicoItemService.save(servicoItem));
	}

	@PutMapping(SERVICOS_ITENS_URL)
	public ResponseEntity atualizarItemDeServico(@Valid @RequestBody ServicoItem servicoItem) {
		if (servicoItemService.findById(servicoItem.getId()) != null)
			return ResponseEntity.ok(servicoItemService.save(servicoItem));
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping(SERVICOS_ITENS_PORID_URL)
	public ResponseEntity deletarServicoItem(@PathVariable("id") Long id) {
		Optional<ServicoItem> vendaADeletar = servicoItemService.findById(id);
		if (vendaADeletar != null) {
			servicoItemService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(SERVICOS_ITENS_PORID_URL)
	public ResponseEntity buscarUmItemDeServico(@PathVariable("id") Long id) {
		return ResponseEntity.ok(servicoItemService.findById(id));
	}

}
