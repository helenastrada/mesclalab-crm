package br.com.mesclalab.crm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "users")
@Data
@NoArgsConstructor
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@Column(nullable = false, unique = true)
	private String username;
	
//	@NotNull
//	@Column(nullable = false, unique = true)
//	private String name;

	@Column(nullable = false, unique = true)
	private String email;

	@NotNull
	private String password;

	@OneToOne
	@JoinColumn(name = "role_id")
	private Role role;
	
	public User(String username, String email, String password, Role role) {
		this.setUsername(username);
		this.setEmail(email);
		this.setPassword(password);
		this.setRole(role);
	}

	public User(Long id, String username, String email, String password, Role role) {
		this(username, email, password, role);
		this.setId(id);
	}
	
	@JsonIgnore
	public String getPassword() {
		return this.password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

}
