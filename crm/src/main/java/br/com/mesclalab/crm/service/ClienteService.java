package br.com.mesclalab.crm.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.mesclalab.crm.entity.Cliente;

@Service
public interface ClienteService {

	Cliente salvar(Cliente cliente);

	void delete(Long id);

	List<Cliente> buscarTodosClientes();

	Optional<Cliente> findById(Long id);

	List<Cliente> findByUser(Long id);
	
	Cliente buscarPorCnpj(String cnpj);
	
}
