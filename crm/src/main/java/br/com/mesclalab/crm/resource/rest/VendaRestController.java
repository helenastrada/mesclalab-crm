package br.com.mesclalab.crm.resource.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.mesclalab.crm.entity.Cliente;
import br.com.mesclalab.crm.entity.Venda;
import br.com.mesclalab.crm.security.auth.JwtUser;
import br.com.mesclalab.crm.service.ClienteService;
import br.com.mesclalab.crm.service.VendaService;
import br.com.mesclalab.crm.utils.WebUtils;

@SuppressWarnings("rawtypes")
@RestController
public class VendaRestController {

	private final static String VENDAS_URL = "/api/vendas";
	private final static String VENDAS_PORID_URL = "/api/vendas/{id}";
	private final static String VENDAS_PORID_TOTAL_URL = "/api/vendas/{id}/total";
	private final static String VENDAS_DO_CLIENTE_URL = "/api/vendas/cliente";

	private VendaService vendaService;
	private ClienteService clienteService;
	
	@Autowired
	private WebUtils webUtils;

	@Autowired
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@Autowired
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@GetMapping(VENDAS_URL)
	public ResponseEntity listarVendas() {
		return ResponseEntity.ok(vendaService.buscarTodasAsVendas());
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@PostMapping(VENDAS_URL)
	public ResponseEntity realizarNovaVenda(@Valid @RequestBody Venda venda) {
		return ResponseEntity.ok(vendaService.salvar(venda));
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@PutMapping(VENDAS_URL)
	public ResponseEntity atualizarVenda(@Valid @RequestBody Venda venda) {
		if (vendaService.findById(venda.getId()) != null)
			return ResponseEntity.ok(vendaService.salvar(venda));
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@DeleteMapping(VENDAS_PORID_URL)
	public ResponseEntity deletarUmaVenda(@PathVariable("id") Long id) {
		Optional<Venda> vendaADeletar = vendaService.findById(id);
		if (vendaADeletar != null) {
			vendaService.deletarVenda(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@GetMapping(VENDAS_PORID_URL)
	public ResponseEntity buscarUmaVenda(@PathVariable("id") Long id) {
		return ResponseEntity.ok(vendaService.findById(id));
	}

	@GetMapping(VENDAS_DO_CLIENTE_URL)
	public ResponseEntity listarVendasParaUmCliente() {
		JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Cliente> codigoDoCliente = clienteService.findByUser(user.getId());
		return ResponseEntity.ok(vendaService.buscarPorIdDoCliente(codigoDoCliente.get(0).getId()));
	}
	
	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	@GetMapping(VENDAS_PORID_TOTAL_URL)
	public ResponseEntity mostrarValorTotalDaCompra(@PathVariable("id") Long id) {
		return ResponseEntity.ok(webUtils.mapaDe("valorTotal", String.format("%1$,.2f", vendaService.calcularValorTotalDaVenda(id))));
	}
	
}
