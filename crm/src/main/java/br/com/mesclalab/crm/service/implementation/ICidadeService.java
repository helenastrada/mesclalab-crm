package br.com.mesclalab.crm.service.implementation;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crm.entity.Cidade;
import br.com.mesclalab.crm.exception.EntityNotFoundException;
import br.com.mesclalab.crm.repository.CidadeRepository;
import br.com.mesclalab.crm.service.CidadeService;

@Service
public class ICidadeService implements CidadeService {

	private CidadeRepository cidadeRepository;

	@Autowired
	public void setCidadeRepository(CidadeRepository cidadeRepository) {
		this.cidadeRepository = cidadeRepository;
	}

	@Override
	public void salvarCidade(Cidade cidade) {
		cidadeRepository.save(cidade);

	}

	@Override
	public Cidade buscarPorNome(String nomeCidade) {
		return cidadeRepository.buscarCidadePorNome(nomeCidade);
	}

	@Override
	public void alterarCidade(Long id, Cidade cidade) {
		
		Cidade cidadeSalvo = buscarPorId(id);
		
		if (cidadeSalvo == null) {
			throw new EntityNotFoundException();
		}
				
		BeanUtils.copyProperties(cidade, cidadeSalvo, "id");
				
		cidadeRepository.save(cidadeSalvo);
		
	}

	@Override
	public List<Cidade> buscarCidades() {
		return cidadeRepository.findAll();
	}

	@Override
	public Cidade buscarPorId(Long id) {
		return cidadeRepository.buscarCidadePorId(id);
	}

}
