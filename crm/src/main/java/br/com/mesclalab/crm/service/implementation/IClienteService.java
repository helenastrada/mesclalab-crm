package br.com.mesclalab.crm.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mesclalab.crm.entity.Cliente;
import br.com.mesclalab.crm.exception.EntityAlreadyExistsException;
import br.com.mesclalab.crm.repository.ClienteRepository;
import br.com.mesclalab.crm.repository.EnderecoRepository;
import br.com.mesclalab.crm.service.ClienteService;

@Service
public class IClienteService implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;

	@Override
	public Cliente salvar(Cliente cliente) throws EntityAlreadyExistsException {
		
		if (buscarPorCnpj(cliente.getCnpj()) != null) {
			throw new EntityAlreadyExistsException();
		}
		
		if (cliente.getEndereco() != null) {
			enderecoRepository.save(cliente.getEndereco());
		}
		
		return clienteRepository.save(cliente);
	}

	@Override
	public void delete(Long id) {
		clienteRepository.deleteById(id);
	}

	@Override
	public List<Cliente> buscarTodosClientes() {
		return clienteRepository.findAll();
	}

	@Override
	public Optional<Cliente> findById(Long id) {
		return clienteRepository.findById(id);
	}

	@Override
	public List<Cliente> findByUser(Long id) {
		return clienteRepository.findByUser(id);
	}

	@Override
	public Cliente buscarPorCnpj(String cnpj) {
		return clienteRepository.buscarPorCnpj(cnpj);
	}

}
