import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import VueRouter from 'vue-router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import rotas from './routes'
import store from './store'

Vue.use(BootstrapVue);
Vue.use(VueRouter);

const router = new VueRouter({
    routes: rotas
});

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
});