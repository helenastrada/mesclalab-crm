import axios from 'axios'

const API_URL = 'http://localhost:9010/';

export default axios.create({
    baseURL: API_URL,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': localStorage.token
    }
})
