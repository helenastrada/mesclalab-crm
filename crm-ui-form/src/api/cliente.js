import api from './api'

export default {
    getData(cb) {
        api.get('/api/clientes').then((response) => {
            setTimeout(() => cb(response.data), 100);
        }, (err) => {
            console.log(err)
        })
    },
    getOneData(cb, mensagem) {
        console.log(mensagem);
        api.get(`/api/clientes/${mensagem}`)
            .then((response) => {
            setTimeout(() => cb(response.data), 100)
            console.log(response.data);
        }, (err) => {
            console.log(err)
        })
    },
    postData(retorno, mCliente) {
        api.post('/api/clientes', mCliente)
            .then(() => {

            }, (err) => {
                console.log(err)
            })
    },
    putData(retorno, mCliente) {
        api.put('/api/clientes', mCliente)
            .then(() => {

            }, (err) => {
                console.log(err)
            })
    }
}