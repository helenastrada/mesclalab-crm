import api from './api'

export default {
    postSignin(cb, mensagem) {
        api.post('/api/auth/signin', mensagem)
            .then((response) => {
            localStorage.setItem('token', 'Bearer ' + response.data.token);
            // localStorage.setItem('expiredDate', (new Date()).toString());
            setTimeout(() => cb(response.data.token), 100)
        }, (err) => {
            console.log(err)
        })
    }
}