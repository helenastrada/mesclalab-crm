import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "./store";

import Admin from './components/admin/Admin.vue';
import Cliente from './components/admin/cliente/Cliente.vue';
import ClienteForm from './components/admin/cliente/ClienteForm.vue';
import Login from './components/auth/Login.vue';
import PageNotFound from './components/default/PageNotFound.vue';

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next()
        return
    }
    next('/admin')
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next()
        return
    }
    next('/login')
};

export default [
    {
        path: '/'
        ,redirect: 'admin'
        ,beforeEnter: ifAuthenticated,
    },
    {
        path: '/admin',
        name: 'Admin',
        component: Admin,
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/clientes',
        name: 'Cliente',
        component: Cliente,
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/clientes/form',
        name: ClienteForm,
        component: ClienteForm,
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/clientes/:id/form',
        name: ClienteForm,
        component: ClienteForm,
        beforeEnter: ifAuthenticated,
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        beforeEnter: ifNotAuthenticated,
    },
    {
        path: '*',
        name: 'PageNotFound',
        component: PageNotFound,
        beforeEnter: ifAuthenticated,
    }

];